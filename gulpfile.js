var gulp = require('gulp'),
    browserify = require('browserify'),
    stylus = require('gulp-stylus'),
    nib = require('nib'),
    minify = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    pug = require('gulp-pug'),
    browserSync = require('browser-sync').create();
gulp.task('build', ['pug','styl', 'js'])
gulp.task('js', function() {
  return gulp.src('./src/js/*.js')
  // .pipe(browserify())
  .pipe(uglify())
  .pipe(gulp.dest('./public/js/')) // en dónde va a estar el archivo destino
})
gulp.task('styl', function() {
  return gulp.src('./src/styles/style.styl') // entry point de styl
    .pipe(stylus({ use: nib() })) //inicializo stylus con nib como plugin
    .pipe(minify())
    .pipe(gulp.dest('./public/css/'));
})
gulp.task('pug', function() {
    return gulp.src('./src/views/*.pug')
        .pipe(pug()) // pip to pug plugin
        .pipe(gulp.dest('./public/')); // tell gulp our output folder
});
gulp.task('watch', function () {
    gulp.watch('./src/js/*.js', ['js']);
    gulp.watch('./src/styles/*.styl', ['styl']);
    gulp.watch('./src/views/*.pug', ['pug']);
});
gulp.task('js-watch', ['js'], function (done) {
    browserSync.reload();
    done();
});
gulp.task('styl-watch', ['styl'], function (done) {
    browserSync.reload();
    done();
});
gulp.task('pug-watch', ['pug'], function (done) {
    browserSync.reload();
    done();
});
gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: "./public/"
        }
    });
    gulp.watch('./src/js/*.js', ['js-watch']);
    gulp.watch('./src/styles/*.styl', ['styl-watch']);
    gulp.watch('./src/views/*.pug', ['pug-watch']);
});